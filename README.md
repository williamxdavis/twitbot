# twitbot

Twitter bot built with [birdy](https://github.com/inueni/birdy).

Requires a text.txt file to generate tweets from and a keys file containing twitter API keys.
