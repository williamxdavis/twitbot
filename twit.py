#!/usr/bin/env python3
from birdy.twitter import UserClient
#from time import sleep
from keys import *
import markov

client = UserClient(CONSUMER_KEY,
        CONSUMER_SECRET,
        ACCESS_TOKEN,
        ACCESS_TOKEN_SECRET)

#sleep(3600)

tweet = markov.speak()

response = client.api.statuses.update.post(status=tweet)
